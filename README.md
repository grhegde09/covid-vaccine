# covid-vaccine

This is a simple python script to check if Covid-19 vaccination slot is available in the next two weeks for a given pincode in India.

## Dependencies
- Python 3

## Instructions
- Usage: `python3 vaccine_slot_checker.py <min_age_limit> <space_separated_pincodes>` Eg: `python vaccine_slot_checker.py 45 581401 581402 581403`

### Recommended setup
- Setup a cron job to run the script every hour. 
- To edit cron jobs, use the command `crontab -e`
- Enter the following into the cron editor.
`0 8-20 * * * /absolute/path/of/python3 /absolute/path/of/vaccine_slot_checker.py 45 581401 581402 581403 2>&1 | tee /tmp/vaccine_slot_checker_op.txt`
- When you receive a notification open `/tmp/vaccine_slot_checker_op.txt` to view the available slots.

 ## :warning: **Notification works only on macOS**
