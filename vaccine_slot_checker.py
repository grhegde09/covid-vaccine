import os
import subprocess
import sys
import datetime
import time
from random import randrange
import random
import logging

# logging.basicConfig(level=logging.INFO)
logging.basicConfig(format='%(asctime)s - %(message)s', level=logging.INFO)

random.seed(datetime.datetime.now())

subprocess.check_call([sys.executable, '-m', 'pip', 'install', 'requests'], stdout=open(os.devnull, 'w'))

import requests

try:
    webhook_key = os.environ["webhook_key"]
except Exception:
    webhook_key = ""


def notify(title, text=""):
    try:
        os.system("""
                  osascript -e 'display notification "{}" with title "{}"'
                  """.format(text, title))
    except Exception:
        pass


def send_telegram(min_age_limit, doses, slots):
    try:
        url = f"https://maker.ifttt.com/trigger/cowin_slot_available/with/key/{webhook_key}?value1={doses}&value2={slots}&value3={min_age_limit}"
        logging.info(f"Webhook URL:{url}")
        requests.get(url)
    except Exception:
        pass


def get_availability_by_pincode(pincode, date_to_check, min_age):
    date_str = date_to_check.strftime('%d-%m-%Y')
    url = f"https://cdn-api.co-vin.in/api/v2/appointment/sessions/public/calendarByPin?pincode={pincode}&date={date_str}"
    # logging.info(f"URL:{url}")

    headers = {
        "Host": "cdn-api.co-vin.in",
        "User-Agent": f"Mozilla/5.0 (Macintosh; Intel Mac OS X 10.15; rv:78.0) Gecko/20100101 Firefox/78.0 Waterfox/78.10.0.{randrange(1000)}",
        "Accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
        "Accept-Language": "en-US,en;q=0.5",
        "Accept-Encoding": "gzip, deflate, br",
        "DNT": "1",
        "Connection": "keep-alive",
        "Upgrade-Insecure-Requests": "1"}
    response = requests.get(url, headers=headers)
    if response.status_code != 200:
        logging.info(f"Non-200 response code {response.status_code} {response.text}")
        logging.info(f"No slots available in {pincode} on {date_str}")
        return 0, 0
    result = response.json()
    if len(result["centers"]) == 0:
        logging.info(f"No slots available in {pincode} on {date_str}")
        return 0, 0
    is_slot_available = False
    available_slots = []
    num_slots = 0
    num_doses = 0
    for center in result["centers"]:
        for session in center["sessions"]:
            if session["min_age_limit"] <= min_age and session["available_capacity"] > 0:
                is_slot_available = True
                slot = {'Center': center['name'],
                        'Block': center['block_name'],
                        'Pin Code': center['pincode'],
                        'Available Doses': session['available_capacity'],
                        'Slots': session['slots'],
                        'Fee Type': center['fee_type'],
                        'Vaccine Type': session['vaccine']}
                available_slots.append(slot)
                num_slots += len(session['slots'])
                num_doses += session['available_capacity']
                logging.info(
                    f"\n\nCenter:{center['name']}\nBlock:{center['block_name']}\nPin Code:{center['pincode']}\nAvailable Doses:{session['available_capacity']}\nSlots:{session['slots']}\nFee Type:{center['fee_type']}\nVaccine Type:{session['vaccine']}")

    if not is_slot_available:
        logging.info(f"No slots available in {pincode} on {date_str}")

    return num_doses, num_slots


def main():
    if len(sys.argv) < 3:
        logging.info(
            "Usage python vaccine_slot_checker.py <min_age_limit> <space_separated_pincodes>\n Eg: python vaccine_slot_checker.py 45 581401 581402 581403")
        sys.exit()

    min_age_limit = int(sys.argv[1])
    logging.info(f"Minimum age limit:{min_age_limit}")
    pincodes = sys.argv[2:]
    num_days_to_look = 14
    dates_to_check = [datetime.datetime.today() + datetime.timedelta(days=x) for x in range(0, num_days_to_look)]
    total_doses = 0
    total_slots = 0
    for pincode in pincodes:
        for date_to_check in dates_to_check:
            try:
                num_doses, num_slots = get_availability_by_pincode(pincode, date_to_check, min_age_limit)
                total_doses += num_doses
                total_slots += num_slots
            except Exception as e:
                logging.info(f"No available slots in {pincode} on {date_to_check}")
                logging.info(e)
            time.sleep(0.5)

    if total_doses > 0:
        notify("Vaccination slot available!", f"{total_doses} doses available in {total_slots} slots")
        send_telegram(min_age_limit, total_doses, total_slots)


if __name__ == '__main__':
    main()
